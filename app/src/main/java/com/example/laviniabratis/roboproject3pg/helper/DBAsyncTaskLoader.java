package com.example.laviniabratis.roboproject3pg.helper;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.laviniabratis.roboproject3pg.model.Contact;
import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

import java.util.Collections;
import java.util.List;

/**
 * Created by lavinia.bratis on 8/19/15.
 */
public abstract class DBAsyncTaskLoader extends AsyncTaskLoader<PhoneContacts> {
    private Context context;
    protected PhoneContacts contacts;

    public DBAsyncTaskLoader(Context context){
        super(context);
        this.context = context;
    }

    protected PhoneContacts getContactsFromDB(){
        List<Contact> contacts = PhoneContactsDatabaseHelper.getInstance(context).getContactsFromDB();
        Collections.sort(contacts, new AlphabeticalComparator());

        return new PhoneContacts(contacts);
    }

    @Override
    public abstract PhoneContacts loadInBackground();

    @Override
    public void onCanceled(PhoneContacts data) {
        super.onCanceled(data);
    }

    @Override
    public void deliverResult(PhoneContacts data) {
        super.deliverResult(data);

        if (isReset()) {
            // An async query came in while the loader is stopped.  We don't need the result.
            if (data != null) {
                data = null;
            }
        }

        if (isStarted()) {
            // If the Loader is currently started, we can immediately
            // deliver its results.
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        if (contacts != null) {
            // If we currently have a result available, deliver it immediately.
            deliverResult(contacts);
        }

        if (takeContentChanged() || contacts == null) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();

        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();
        // Release the resources associated with 'data' if needed.
        if (contacts != null) {
            contacts = null;
        }
    }
}
