package com.example.laviniabratis.roboproject3pg.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.laviniabratis.roboproject3pg.R;
import com.example.laviniabratis.roboproject3pg.helper.AddToDBAsyncTaskLoader;
import com.example.laviniabratis.roboproject3pg.helper.ContactsCursorAdapter;
import com.example.laviniabratis.roboproject3pg.helper.RemoveFromDBAsyncTaskLoader;
import com.example.laviniabratis.roboproject3pg.helper.SearchAsyncTaskLoader;
import com.example.laviniabratis.roboproject3pg.model.Contact;
import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavinia.bratis on 8/5/15.
 */
public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor>{

    private EditText searchInput;
    private RecyclerView contactsList;
    private FloatingActionButton addContactBtn;
    private ContactsCursorAdapter mCursorAdapter;
    private ProgressDialog progressDialog;

    // Defines a variable for the search string
    private String mSearchString;

    private static final String[] PROJECTION =
            {
                    ContactsContract.CommonDataKinds.Phone._ID,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Phone.NUMBER
            };

    private TextWatcher tw;

    private PhoneContacts allPhoneContacts, phoneContacts;

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);

        setContentView(R.layout.main_activity);

        setLayout();
    }

    private void setLayout(){
        searchInput = (EditText) findViewById(R.id.searchInput);
        contactsList = (RecyclerView) findViewById(R.id.contactsList);
        addContactBtn = (FloatingActionButton) findViewById(R.id.add_button);

        addContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(MainActivity.this);
                Window window = dialog.getWindow();
                window.requestFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.add_contact);
                window.setLayout((int) getResources().getDimension(R.dimen.add_contact_dialog_width), (int) getResources().getDimension(R.dimen.add_contact_dialog_height));

                final EditText nameEdt = (EditText)dialog.findViewById(R.id.contact_name);
                final EditText phoneNumberEdt = (EditText)dialog.findViewById(R.id.phone_number);

                Button dialogButton = (Button) dialog.findViewById(R.id.ok_btn);

                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String name = nameEdt.getText().toString();
                        String phoneNumber = phoneNumberEdt.getText().toString();
                        if (name.isEmpty()){
                            nameEdt.setError(getResources().getString(R.string.name_error));
                        }
                        if (phoneNumber.isEmpty()){
                            phoneNumberEdt.setError(getResources().getString(R.string.phone_error));
                        }
                        if (!name.isEmpty() && !phoneNumber.isEmpty()){
                            Snackbar.make(contactsList, getResources().getString(R.string.add_message_snackbar), Snackbar.LENGTH_LONG).show();
                            loadContacts(true, new PhoneContacts(new Contact(name, phoneNumber)));
                            dialog.dismiss();
                        }
                    }
                });

                dialog.show();
            }
        });

        setSearchContactWatcher();
        setContactsList();
        setDismissContactAction();
    }

    // Load contacts from DB after add contacts / remove contact
    private void loadContacts(final boolean addContacts, final PhoneContacts phone_contacts){
        getLoaderManager().restartLoader(2, null,
                new LoaderManager.LoaderCallbacks<PhoneContacts>() {
                    @Override
                    public Loader<PhoneContacts> onCreateLoader(int id, Bundle args) {
                        if (addContacts){
                            return new AddToDBAsyncTaskLoader(MainActivity.this, phone_contacts);
                        }
                        else {
                            return new RemoveFromDBAsyncTaskLoader(MainActivity.this, phone_contacts);
                        }
                    }

                    @Override
                    public void onLoadFinished(Loader<PhoneContacts> loader, PhoneContacts data) {
                        phoneContacts = data;
                        mCursorAdapter.updatePhoneContacts(phoneContacts);
                    }

                    @Override
                    public void onLoaderReset(Loader<PhoneContacts> loader) {
                        mCursorAdapter.updatePhoneContacts(null);
                    }
                }
        );
    }

    private void setSearchContactWatcher(){
        tw = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                mSearchString = searchInput.getText().toString();
                if (!mSearchString.equals("")) {
                    SearchAsyncTaskLoader filterLoader = new SearchAsyncTaskLoader(MainActivity.this, allPhoneContacts, mSearchString);
                    phoneContacts = filterLoader.loadInBackground();
                } else {
                    phoneContacts = allPhoneContacts;
                }
                mCursorAdapter.updatePhoneContacts(phoneContacts);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after){
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        };

        searchInput.addTextChangedListener(tw);
    }

    private void setContactsList(){
        contactsList.setHasFixedSize(true);
        contactsList.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
            }
        });
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        contactsList.setLayoutManager(mLayoutManager);

        // Initializes the loader
        getLoaderManager().initLoader(0, null, this);

        // set adapter
        mCursorAdapter = new ContactsCursorAdapter(MainActivity.this, allPhoneContacts);
        contactsList.setAdapter(mCursorAdapter);
    }

    private void setDismissContactAction(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                Snackbar.make(contactsList, getResources().getString(R.string.remove_message_snackbar), Snackbar.LENGTH_LONG).show();
                loadContacts(false, new PhoneContacts(phoneContacts.getContact(viewHolder.getAdapterPosition())));
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);

        itemTouchHelper.attachToRecyclerView(contactsList);
    }

    @Override
    public CursorLoader onCreateLoader(int id, Bundle args) {
        progressDialog = new ProgressDialog(MainActivity.this, R.style.StyledDialog);
        progressDialog.show();
        Uri baseUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        return new CursorLoader(
                MainActivity.this,
                baseUri,
                PROJECTION,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data){
        allPhoneContacts = new PhoneContacts(extractPhoneContacts(data));
        phoneContacts = allPhoneContacts;

        progressDialog.dismiss();

        // Save to DB and update contacts list
        loadContacts(true, phoneContacts);
    }

    private List<Contact> extractPhoneContacts(Cursor data){
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        while (data.moveToNext()){
            contacts.add(fromCursor(data));
        }

        return contacts;
    }

    public Contact fromCursor(Cursor data) {
        String name = data.getString(data.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
        String phoneNumber = data.getString(data.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

        return new Contact(name, phoneNumber);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.updatePhoneContacts(allPhoneContacts);
    }
}
