package com.example.laviniabratis.roboproject3pg.model;

import android.content.Context;
import android.provider.ContactsContract;

import com.example.laviniabratis.roboproject3pg.helper.AlphabeticalComparator;
import com.example.laviniabratis.roboproject3pg.helper.DBAsyncTaskLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by lavinia.bratis on 8/6/15.
 */
public class PhoneContacts {

    private List<Contact> phoneContacts;

    public PhoneContacts(Contact contact){
        this.phoneContacts = new ArrayList<>();
        phoneContacts.add(contact);
    }

    public PhoneContacts(List<Contact> contacts){
        this.phoneContacts = contacts;
    }

    public Contact getContact(int position){
        return phoneContacts.get(position);
    }

    public int getNumberOfContacts(){
        if (phoneContacts != null)
            return phoneContacts.size();
        return 0;
    }

    public PhoneContacts filterContactsByName(String name){

        ArrayList<Contact> filteredContacts = new ArrayList<>();

        for (Contact contact : phoneContacts){
            if (contact.getName().regionMatches(true, 0, name, 0, name.length())){
                filteredContacts.add(contact);
            }
        }

        return new PhoneContacts(filteredContacts);
    }

    public PhoneContacts filterContactsByPhoneNumber(String phoneNumber){

        ArrayList<Contact> filteredContacts = new ArrayList<>();

        for (Contact contact : phoneContacts){
            if (contact.getPhoneNumber().startsWith(phoneNumber)){
                filteredContacts.add(contact);
            }
        }

        return new PhoneContacts(filteredContacts);
    }
//
//    public PhoneContacts removeContact(int position) {
//        Contact contact = phoneContacts.get(position);;
//        if (position >= 0 && position < phoneContacts.size()) {
//            // remove from DB
//            contact.delete();
//        }
//
//        return new PhoneContacts(contact);
//    }

//    public PhoneContacts addContact(Context context, Contact contact){
//        // save to DB
//        ArrayList<Contact> contacts = new ArrayList<>();
//        contacts.add(contact);
//        DBAsyncTaskLoader dbLoader = new DBAsyncTaskLoader(context, contacts);
//        dbLoader.loadInBackground();
//        // get phone contacts
//        return getPhoneContactsFromDB();
//    }

}
