package com.example.laviniabratis.roboproject3pg.helper;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

import java.util.regex.Pattern;

/**
 * Created by lavinia.bratis on 8/6/15.
 */
public class SearchAsyncTaskLoader extends AsyncTaskLoader<PhoneContacts> {
    private PhoneContacts phoneContacts;
    private String searchString;

    public SearchAsyncTaskLoader(Context context, PhoneContacts phoneContacts, String searchString) {
        super(context);
        this.phoneContacts = phoneContacts;
        this.searchString = searchString;
    }

    @Override
    public PhoneContacts loadInBackground() {

        PhoneContacts pc;
        if (Pattern.matches("[a-zA-Z]+", searchString))
            pc = phoneContacts.filterContactsByName(searchString);
        else
            pc = phoneContacts.filterContactsByPhoneNumber(searchString);
        return pc;
    }
}
