package com.example.laviniabratis.roboproject3pg.helper;

import android.content.Context;

import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

/**
 * Created by lavinia.bratis on 8/19/15.
 */
public class RemoveFromDBAsyncTaskLoader extends DBAsyncTaskLoader {
    private Context context;
    private PhoneContacts removedContacts;

    public RemoveFromDBAsyncTaskLoader(Context context, PhoneContacts contacts){
        super(context);
        this.context = context;
        this.removedContacts = contacts;
    }

    @Override
    public PhoneContacts loadInBackground() {
        PhoneContactsDatabaseHelper.getInstance(context).removeContactFromDB(removedContacts);

        contacts = getContactsFromDB();
        return contacts;
    }
}
