package com.example.laviniabratis.roboproject3pg.helper;

import android.content.Context;
import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

/**
 * Created by lavinia.bratis on 8/19/15.
 */
public class AddToDBAsyncTaskLoader extends DBAsyncTaskLoader {
    private Context context;
    private PhoneContacts addedContacts;

    public AddToDBAsyncTaskLoader(Context context, PhoneContacts contacts){
        super(context);
        this.context = context;
        this.addedContacts = contacts;
    }

    @Override
    public PhoneContacts loadInBackground() {
        PhoneContactsDatabaseHelper.getInstance(context).addContactToDB(addedContacts);

        contacts = getContactsFromDB();
        return contacts;
    }




}
