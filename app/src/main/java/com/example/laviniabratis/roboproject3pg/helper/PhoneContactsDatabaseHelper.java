package com.example.laviniabratis.roboproject3pg.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.laviniabratis.roboproject3pg.model.Contact;
import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lavinia.bratis on 8/19/15.
 */
public class PhoneContactsDatabaseHelper extends SQLiteOpenHelper {
    private static PhoneContactsDatabaseHelper dbHelper;

    // Database Info
    private static final String DATABASE_NAME = "phoneContactsDatabase";
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE_PHONE_CONTACTS = "phone_contacts";

    // Post Table Columns
    private static final String PHONE_CONTACTS_ID = "id";
    private static final String PHONE_CONTACTS_NAME = "name";
    private static final String PHONE_CONTACTS_PHONE_NUMBER = "phone_number";

    private PhoneContactsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized PhoneContactsDatabaseHelper getInstance(Context context){
        if (dbHelper == null)
            dbHelper = new PhoneContactsDatabaseHelper(context);

        return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_PHONE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_PHONE_CONTACTS +
                "(" +
                PHONE_CONTACTS_NAME + " TEXT, " +
                PHONE_CONTACTS_PHONE_NUMBER + " TEXT, " +
                "UNIQUE(" + PHONE_CONTACTS_NAME + "," + PHONE_CONTACTS_PHONE_NUMBER + ")" +
                ")";

        db.execSQL(CREATE_PHONE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addContactToDB(PhoneContacts contacts){
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        for (int i = 0; i < contacts.getNumberOfContacts(); i++) {
            if (checkContact(contacts.getContact(i))) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(PHONE_CONTACTS_NAME, contacts.getContact(i).getName());
                contentValues.put(PHONE_CONTACTS_PHONE_NUMBER, contacts.getContact(i).getPhoneNumber());

                db.insert(TABLE_PHONE_CONTACTS, null, contentValues);
            }
        }

        db.close();
    }

    private boolean checkContact(Contact contact){
        // Gets the data repository in read mode
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_PHONE_CONTACTS + " WHERE "
                + PHONE_CONTACTS_NAME + " = '" + contact.getName()
                + "' AND "
                + PHONE_CONTACTS_PHONE_NUMBER + " = '" + contact.getPhoneNumber() + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() != 0)
            return false;

        return true;
    }

    public void removeContactFromDB(PhoneContacts contacts){
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        for (int i = 0; i < contacts.getNumberOfContacts(); i++) {
            String whereClause = PHONE_CONTACTS_NAME + " LIKE ? " + " AND " + PHONE_CONTACTS_PHONE_NUMBER + " LIKE ? ";
            String selectionArgs[] = {contacts.getContact(i).getName(), contacts.getContact(i).getPhoneNumber()};
            db.delete(TABLE_PHONE_CONTACTS, whereClause, selectionArgs);
        }

        db.close();
    }

    public List<Contact> getContactsFromDB(){
        // Gets the data repository in read mode
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_PHONE_CONTACTS;
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<Contact> contacts = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                contacts.add(getContact(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return contacts;
    }

    public Contact getContact(Cursor c){
        String name = c.getString(c.getColumnIndex(PHONE_CONTACTS_NAME));
        String phoneNumber = c.getString(c.getColumnIndex(PHONE_CONTACTS_PHONE_NUMBER));

        return new Contact(name, phoneNumber);
    }
}
