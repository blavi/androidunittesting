package com.example.laviniabratis.roboproject3pg.helper;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.laviniabratis.roboproject3pg.R;
import com.example.laviniabratis.roboproject3pg.model.Contact;
import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

import java.util.List;

/**
 * Created by lavinia.bratis on 8/5/15.
 */
public class ContactsCursorAdapter extends RecyclerView.Adapter<ContactsCursorAdapter.ViewHolder> {

    private PhoneContacts phoneContacts;

    public ContactsCursorAdapter(Context context, PhoneContacts phoneContacts){
        this.phoneContacts = phoneContacts;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView contactNameView, phoneNumberView;
        public ViewHolder(View view) {
            super(view);
            contactNameView = (TextView) view.findViewById(R.id.name);
            phoneNumberView = (TextView) view.findViewById(R.id.phone_number);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Contact listItem = phoneContacts.getContact(position);
        viewHolder.contactNameView.setText(listItem.getName());
        viewHolder.phoneNumberView.setText(listItem.getPhoneNumber());
    }

    @Override
    public ContactsCursorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);
        ViewHolder vh = new ViewHolder(layout);

        return vh;
    }

    @Override
    public int getItemCount() {
        if (phoneContacts != null)
            return phoneContacts.getNumberOfContacts();
        return 0;
    }

    public void updatePhoneContacts(PhoneContacts newPhoneContacts){
        phoneContacts = newPhoneContacts;
        this.notifyDataSetChanged();
    }
}
