package com.example.laviniabratis.roboproject3pg.helper;

import com.example.laviniabratis.roboproject3pg.model.Contact;

import java.util.Comparator;

/**
 * Created by lavinia.bratis on 8/19/15.
 */
public class AlphabeticalComparator implements Comparator<Contact> {
    @Override
    public int compare(Contact c1, Contact c2) {

        return c1.getName().compareTo(c2.getName());
    }
}
