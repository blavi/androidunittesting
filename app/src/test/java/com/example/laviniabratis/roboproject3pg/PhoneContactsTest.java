package com.example.laviniabratis.roboproject3pg;

import com.example.laviniabratis.roboproject3pg.model.Contact;
import com.example.laviniabratis.roboproject3pg.model.PhoneContacts;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by lavinia.bratis on 8/7/15.
 */

public class PhoneContactsTest{
    static PhoneContacts pc;

    @Before
    public void setPhoneContacts(){
        List<Contact> list = new ArrayList<>();

        list.add(new Contact("Ana", "07329302932"));
        list.add(new Contact("Oana", "07323090922"));
        list.add(new Contact("Cosmin", "0723222233"));
        list.add(new Contact("Dan", "07423255343"));
        list.add(new Contact("Andrei", "0753343l433"));
        list.add(new Contact("Elena", "07121223333"));
        list.add(new Contact("Cosmina", "0753209329"));
        list.add(new Contact("Maria", "0763404300"));
        list.add(new Contact("Bogdan", "0773293293"));
        list.add(new Contact("Claudiu", "07723920392"));
        list.add(new Contact("Larisa", "07694309430"));
        list.add(new Contact("Lavinia", "07529309434"));

        Collections.sort(list, new Comparator<Contact>() {
            @Override
            public int compare(Contact c1, Contact c2) {
                return c1.getName().compareToIgnoreCase(c2.getName());
            }
        });

        pc = new PhoneContacts(list);
    }

    @Test
    public void testNumberOfContacts(){
        Assert.assertEquals(12, pc.getNumberOfContacts());
    }

    @Test
    public void testGetContact(){
        Contact c = new Contact("Lavinia", "07529309434");
        Assert.assertTrue(c.equals(pc.getContact(9)));
    }

    @Test
    public void testFilteredContactsByName_A(){
        PhoneContacts contacts = pc.filterContactsByName("A");
        Assert.assertEquals(2, contacts.getNumberOfContacts());
        Contact c1 = new Contact("Ana", "07329302932");
        Contact c2 = new Contact("Andrei", "0753343l433");
        Assert.assertTrue(c1.equals(contacts.getContact(0)));
        Assert.assertTrue(c2.equals(contacts.getContact(1)));
    }

    @Test
    public void testFilteredContactsByName_Cosmin(){
        PhoneContacts contacts =  pc.filterContactsByName("Cosmin");
        Assert.assertEquals(2, contacts.getNumberOfContacts());
        Contact c1 = new Contact("Cosmin", "0723222233");
        Contact c2 = new Contact("Cosmina", "0753209329");
        Assert.assertTrue(c1.equals(contacts.getContact(0)));
        Assert.assertTrue(c2.equals(contacts.getContact(1)));
    }

    @Test
    public void testFilteredContactsByName_V(){
        PhoneContacts contacts = pc.filterContactsByName("V");
        Assert.assertEquals(0, contacts.getNumberOfContacts());
    }

    @Test
    public void testFilteredContactsByName_NoName(){
        Assert.assertEquals(12, pc.filterContactsByName("").getNumberOfContacts());
    }

    @Test
    public void testFilteredContactsByPhoneNumber_NoPhhoneNumber(){
        Assert.assertEquals(12, pc.filterContactsByPhoneNumber("").getNumberOfContacts());
    }

    @Test
    public void testFilteredContactsByPhoneNumber_078(){
        PhoneContacts contacts = pc.filterContactsByPhoneNumber("078");
        Assert.assertEquals(0, contacts.getNumberOfContacts());
    }

    @Test
    public void testFilteredContactsByPhoneNumber_07(){
        PhoneContacts contacts = pc.filterContactsByPhoneNumber("07");
        Assert.assertEquals(12, contacts.getNumberOfContacts());
        Assert.assertTrue(pc.getContact(0).equals(contacts.getContact(0)));
        Assert.assertTrue(pc.getContact(1).equals(contacts.getContact(1)));
        Assert.assertTrue(pc.getContact(2).equals(contacts.getContact(2)));
        Assert.assertTrue(pc.getContact(3).equals(contacts.getContact(3)));
        Assert.assertTrue(pc.getContact(4).equals(contacts.getContact(4)));
        Assert.assertTrue(pc.getContact(5).equals(contacts.getContact(5)));
        Assert.assertTrue(pc.getContact(6).equals(contacts.getContact(6)));
        Assert.assertTrue(pc.getContact(7).equals(contacts.getContact(7)));
        Assert.assertTrue(pc.getContact(8).equals(contacts.getContact(8)));
        Assert.assertTrue(pc.getContact(9).equals(contacts.getContact(9)));
        Assert.assertTrue(pc.getContact(10).equals(contacts.getContact(10)));
        Assert.assertTrue(pc.getContact(11).equals(contacts.getContact(11)));
    }

    @Test
    public void testFilteredContactsByPhoneNumber_0732(){
        PhoneContacts contacts = pc.filterContactsByPhoneNumber("0732");
        Assert.assertEquals(2, contacts.getNumberOfContacts());
        Contact c1 = new Contact("Ana", "07329302932");
        Contact c2 = new Contact("Oana", "07323090922");
        Assert.assertTrue(c1.equals(contacts.getContact(0)));
        Assert.assertTrue(c2.equals(contacts.getContact(1)));
    }


//    public void testAddingContact_ValidInput(){
//        pc.addContact(new Contact("Bogdana", "0755505425"));
//        Assert.assertEquals(13, pc.getNumberOfContacts());
//        Assert.assertEquals("Bogdana", pc.getContact(3).getName());
//        Assert.assertEquals("0755505425", pc.getContact(3).getPhoneNumber());
//    }


    public void testAddingContact_InvalidName(){
    //    pc.addContact(new Contact("", "0721980337"));
        Assert.assertEquals(12, pc.getNumberOfContacts());
    }


    public void testAddingContact_InvalidPhoneNumber(){
    //    pc.addContact(new Contact("Andreea", ""));
        Assert.assertEquals(12, pc.getNumberOfContacts());
    }


//    public void testRemoveContact_ValidPosition(){
//        PhoneContacts contacts = pc.removeContact(3);
//        Assert.assertEquals(11, contacts.getNumberOfContacts());
//    }


//    public void testRemoveContact_InvalidPosition(){
//        PhoneContacts contacts = pc.removeContact(-1);
//        Assert.assertEquals(12, contacts.getNumberOfContacts());
//    }

}
